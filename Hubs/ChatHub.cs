﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace chat.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendToAll(string name, string message, string chatRoomName)
        {
            await Clients.Group(chatRoomName).SendAsync("sendToAll", name, message);
        }

        public async Task JoinChatRoom(string chatRoomName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, chatRoomName);
        }
    }
}
