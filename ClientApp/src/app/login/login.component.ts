import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public userName = '';
  public chatRoomName = '';
  public chatRoomNames = ['first chat', 'second chat'];

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  public logIn = () => {
    if (this.userName && this.chatRoomName) {
      this.authService.logIn(this.userName, this.chatRoomName);
    }
  }

}
