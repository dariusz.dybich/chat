import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { AuthService } from '../shared/services/auth.service';
import { map, tap } from 'rxjs/operators';
import { Message } from '../models/message.model';
import { User } from '../models/user.model';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {

  private chatHubConnection: HubConnection;

  @ViewChild('newMessageInput')
  private newMessageInput: ElementRef;

  public newMessage = '';
  public currentUser: User = null;
  public messages: Array<Message> = [];

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.newMessageInput.nativeElement.focus();

    const hubConnectionBuilder = new HubConnectionBuilder();
    this.chatHubConnection = hubConnectionBuilder
      .withUrl('/chat')
      .build();

    this.chatHubConnection
      .start()
      .then(() => {
        this.authService.getUser().pipe(
          tap(user => {
            this.currentUser = user;
            this.chatHubConnection.invoke('JoinChatRoom', this.currentUser.chatRoomName);
          })
        ).subscribe();
      })
      .catch(err => console.log('Error while establishing connection :('));

    this.chatHubConnection.on('sendToAll', (userName: string, receivedMessage: string) => {
      this.addMessage(userName, receivedMessage);
    });
  }

  public sendMessage = () => {
    if (!this.newMessage) {
      return;
    }

    this.chatHubConnection
      .invoke('sendToAll', this.currentUser.userName, this.newMessage, this.currentUser.chatRoomName)
      .catch(err => console.error(err));

    this.newMessageInput.nativeElement.innerText = '';
    this.newMessage = '';
  }

  private addMessage = (userName: string, text: string) => {
    const message: Message = {
      text,
      userName
    };

    this.messages.push(message);
  }

  ngOnDestroy(): void {
    if (this.chatHubConnection) {
      this.chatHubConnection.stop();
    }
  }

}
