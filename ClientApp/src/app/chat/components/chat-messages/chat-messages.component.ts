import { Component, OnInit, ViewChild, ElementRef, Input, QueryList, ViewChildren, AfterViewInit } from '@angular/core';
import { Message } from '../../../models/message.model';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-chat-messages',
  templateUrl: './chat-messages.component.html',
  styleUrls: ['./chat-messages.component.scss']
})
export class ChatMessagesComponent implements OnInit, AfterViewInit {

  @ViewChildren('messagesList') 
  messagesList: QueryList<any>;

  @Input()
  public messages: Message[];

  @Input()
  userName: string;

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {
    setTimeout(() => {
      this.scrollToBottom();
    }, 0);
  }

  ngAfterViewInit(): void {
    this.messagesList.changes
      .pipe(
        tap(() => {
          this.scrollToBottom();
        })
      )
      .subscribe();
  }

  public hasNextMessageSameAuthor = (message: Message, messageIndex: number): boolean => {
    if ((messageIndex + 1) === this.messages.length) {
      return false;
    } else {
      const nextMessage = this.messages[messageIndex + 1];
      return nextMessage.userName === message.userName;
    }
  }

  public hasPrevMessageSameAuthor = (message: Message, messageIndex: number): boolean => {
    if (messageIndex === 0) {
      return false;
    } else {
      const prevMessage = this.messages[messageIndex - 1];
      return prevMessage.userName === message.userName;
    }
  }

  private scrollToBottom = () => {
    this.elementRef.nativeElement.scrollTop = this.elementRef.nativeElement.scrollHeight;
  }

}
