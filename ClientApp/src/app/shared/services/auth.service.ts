import { Injectable } from '@angular/core';
import { User } from '../../models/user.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user: User = null;
  private user$: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor(private router: Router) { }

  public logIn(userName: string, chatRoomName: string) {
    this.user = { userName, chatRoomName };

    this.user$.next(this.user);
    this.router.navigate(['']);
  }

  public getUser(): Observable<User> {
    return this.user$.asObservable();
  }

}
